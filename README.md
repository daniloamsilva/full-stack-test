# Teste Desenvolvedor Full Stack

O teste consiste em criar uma aplicação com Backend(Laravel || NodeJS) que expõe uma API REST de um CRUD de usuários e filmes e uma aplicação web contendo uma interface(React/Next.JS) para login e acesso a dados de uma API externa.

![capa](image.png)

## Watch Movies

Acesse o deploy na **Vercel** [aqui](https://full-stack-test-ashy.vercel.app/signin) e utilize as credênciais:
```
email: admin@admin.com
senha: admin
```

<p>
  <img src="https://img.shields.io/badge/18.2-ReactJS-blue" alt="ReactJS Version" />
  <img src="https://img.shields.io/badge/10.0-NestJS-red" alt="NestJS Version" />
  <img src="https://img.shields.io/badge/5.1.3-Typescript-blue" alt="Typescript Version" />
  <img src="https://img.shields.io/badge/5.4.2-Prisma-blueviolet" alt="Prisma Version" />
  <img src="https://img.shields.io/badge/29.5-Jest-success" alt="Jest Version" />
</p>

## Back-end

- Entre na pasta **/backend**

```
cd backend
```

- Crie um arquivo **.env** na raiz do projeto utilizando o **.env.example** como base

```
cp .env.example .env
```

- Instale as dependências

```
npm install
```

- Se estiver usando o docker, crie os containers

```
docker-compose up -d
```

- Dispare as migrations para criar o banco de dados

```
npx prisma migrate dev
```

- Dispare a seed para popular o banco de dados

```
npx prisma db seed
```

- Inicie a aplicação

```
npm run start:dev
```

### Testando

- Todos os testes

```
npm run test
```

- Testes unitários

```
npm run test service
```

- Testes de integração

```
npm run test controller
```

## Front-end

- Entre na pasta **/frontend**

```
cd frontend
```

- Crie um arquivo **.env** na raiz do projeto utilizando o **.env.example** como base

```
cp .env.example .env
```

- Instale as dependências

```
npm install
```

- Inicie a aplicação

```
npm run dev
```

- Navegue para a url **http://localhost:5173**
- Utilize as credênciais:

```
email: admin@admin.com
senha: admin
```
