import NodeEnvironment from 'jest-environment-node';
import { config } from 'dotenv';
import { execSync } from 'node:child_process';
import { JestEnvironmentConfig } from '@jest/environment';

config({ path: '.env' });

function generateDatabaseURL(database: string): string {
  if (!process.env.DATABASE_URL) {
    throw new Error('Please provide a DATABASE_URL environment variable.');
  }

  const url = new URL(process.env.DATABASE_URL);
  const pathnameParts = url.pathname.split('/');
  pathnameParts[pathnameParts.length - 1] = database;
  url.pathname = pathnameParts.join('/');
  return url.toString();
}

export default class PrismaTestEnvironment extends NodeEnvironment {
  private database: string;
  private connectionString: string;
  private isE2ETest: boolean;

  constructor(config: JestEnvironmentConfig, context: any) {
    super(config, context);

    this.database = process.env.DB_TEST_DATABASE;
    this.connectionString = generateDatabaseURL(this.database);

    const regex = new RegExp('.controller.spec.ts$');
    this.isE2ETest = regex.test(context.testPath);
  }

  async setup() {
    if (this.isE2ETest) {
      process.env.DATABASE_URL = this.connectionString;
      this.global.process.env.DATABASE_URL = this.connectionString;
      execSync(`npx prisma migrate dev --skip-generate`);
      execSync(`npx prisma db seed`);
    }

    return super.setup();
  }
}
