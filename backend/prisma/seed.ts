import * as bcrypt from 'bcrypt';
import { PrismaClient } from '@prisma/client';
import { randomUUID } from 'node:crypto';

import { MovieEntity } from '../src/entities/movie.entity';
import { UserEntity } from '@/entities/user.entity';

const prisma = new PrismaClient();

async function main() {
  await prisma.movie.deleteMany({ where: {} });
  await prisma.user.deleteMany({ where: {} });

  const users: UserEntity[] = [
    {
      id: randomUUID(),
      email: 'admin@admin.com',
      password: await bcrypt.hash('admin', 10),
    },
    {
      id: randomUUID(),
      email: 'user1@email.com',
      password: await bcrypt.hash('user1', 10),
    },
    {
      id: randomUUID(),
      email: 'user2@email.com',
      password: await bcrypt.hash('user2', 10),
    },
    {
      id: randomUUID(),
      email: 'user3@email.com',
      password: await bcrypt.hash('user3', 10),
    },
    {
      id: randomUUID(),
      email: 'user4@email.com',
      password: await bcrypt.hash('user4', 10),
    },
    {
      id: randomUUID(),
      email: 'user5@email.com',
      password: await bcrypt.hash('user5', 10),
    },
    {
      id: randomUUID(),
      email: 'user6@email.com',
      password: await bcrypt.hash('user6', 10),
    },
    {
      id: randomUUID(),
      email: 'user7@email.com',
      password: await bcrypt.hash('user7', 10),
    },
    {
      id: randomUUID(),
      email: 'user8@email.com',
      password: await bcrypt.hash('user8', 10),
    },
    {
      id: randomUUID(),
      email: 'user9@email.com',
      password: await bcrypt.hash('user9', 10),
    },
    {
      id: randomUUID(),
      email: 'user10@email.com',
      password: await bcrypt.hash('user10', 10),
    },
    {
      id: randomUUID(),
      email: 'user11@email.com',
      password: await bcrypt.hash('user11', 10),
    },
    {
      id: randomUUID(),
      email: 'user12@email.com',
      password: await bcrypt.hash('user12', 10),
    },
    {
      id: randomUUID(),
      email: 'user13@email.com',
      password: await bcrypt.hash('user13', 10),
    },
    {
      id: randomUUID(),
      email: 'user14@email.com',
      password: await bcrypt.hash('user14', 10),
    },
    {
      id: randomUUID(),
      email: 'user15@email.com',
      password: await bcrypt.hash('user15', 10),
    },
    {
      id: randomUUID(),
      email: 'user16@email.com',
      password: await bcrypt.hash('user16', 10),
    },
    {
      id: randomUUID(),
      email: 'user17@email.com',
      password: await bcrypt.hash('user17', 10),
    },
    {
      id: randomUUID(),
      email: 'user18@email.com',
      password: await bcrypt.hash('user18', 10),
    },
    {
      id: randomUUID(),
      email: 'user19@email.com',
      password: await bcrypt.hash('user19', 10),
    },
    {
      id: randomUUID(),
      email: 'user20@email.com',
      password: await bcrypt.hash('user20', 10),
    },
  ];

  for (const user of users) {
    await prisma.user.upsert({
      where: { email: user.email },
      update: {},
      create: {
        email: user.email,
        password: user.password,
      },
    });
  }

  const movies: MovieEntity[] = [
    {
      id: randomUUID(),
      title: 'The Shawshank Redemption',
      year: 1994,
      poster:
        'https://m.media-amazon.com/images/M/MV5BNDE3ODcxYzMtY2YzZC00NmNlLWJiNDMtZDViZWM2MzIxZDYwXkEyXkFqcGdeQXVyNjAwNDUxODI@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Godfather',
      year: 1972,
      poster:
        'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Dark Knight',
      year: 2008,
      poster:
        'https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Godfather: Part II',
      year: 1974,
      poster:
        'https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: '12 Angry',
      year: 1957,
      poster:
        'https://m.media-amazon.com/images/M/MV5BMWU4N2FjNzYtNTVkNC00NzQ0LTg0MjAtYTJlMjFhNGUxZDFmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Lord of the Rings: The Return of the King',
      year: 2003,
      poster:
        'https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'Pulp Fiction',
      year: 1994,
      poster:
        'https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Good, the Bad and the Ugly',
      year: 1966,
      poster:
        'https://m.media-amazon.com/images/M/MV5BNjJlYmNkZGItM2NhYy00MjlmLTk5NmQtNjg1NmM2ODU4OTMwXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Lord of the Rings: The Fellowship of the Ring',
      year: 2001,
      poster:
        'https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'Forrest Gump',
      year: 1994,
      poster:
        'https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'Inception',
      year: 2010,
      poster:
        'https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Lord of the Rings: The Two Towers',
      year: 2002,
      poster:
        'https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'Star Wars: Episode V - The Empire Strikes Back',
      year: 1980,
      poster:
        'https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Matrix',
      year: 1999,
      poster:
        'https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'Goodfellas',
      year: 1990,
      poster:
        'https://m.media-amazon.com/images/M/MV5BY2NkZjEzMDgtN2RjYy00YzM1LWI4ZmQtMjIwYjFjNmI3ZGEwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: "One Flew Over the Cuckoo's Nest",
      year: 1975,
      poster:
        'https://m.media-amazon.com/images/M/MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
    },
    {
      id: randomUUID(),
      title: 'The Silence of the Lambs',
      year: 1991,
      poster:
        'https://m.media-amazon.com/images/M/MV5BNjNhZTk0ZmEtNjJhMi00YzFlLWE1MmEtYzM1M2ZmMGMwMTU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg',
    },
  ];

  for (const movie of movies) {
    await prisma.movie.upsert({
      where: { title: movie.title },
      update: {},
      create: {
        title: movie.title,
        year: movie.year,
        poster: movie.poster,
      },
    });
  }
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
