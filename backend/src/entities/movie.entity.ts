import { Movie } from '@prisma/client';
import { randomUUID } from 'node:crypto';

export class MovieEntity implements Movie {
  id: string;
  title: string;
  year: number;
  poster: string;

  constructor() {
    if (!this.id) {
      this.id = randomUUID();
    }
  }
}
