import { User } from '@prisma/client';
import { Exclude } from 'class-transformer';
import { randomUUID } from 'node:crypto';

export class UserEntity implements User {
  id: string;
  email: string;
  @Exclude()
  password: string;

  constructor() {
    if (!this.id) {
      this.id = randomUUID();
    }
  }
}
