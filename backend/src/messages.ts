export const Message = {
  USER_INVALID_CREDENTIALS: 'E-mail ou senha estão incorretos',
  USER_NOT_FOUND: 'Usuário não encontrado',
  MOVIE_NOT_FOUND: 'Filme não encontrado',
} as const;
