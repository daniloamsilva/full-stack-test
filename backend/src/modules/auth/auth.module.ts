import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { SigninService } from './use-cases/signin/signin.service';
import { LocalStrategy } from './use-cases/signin/local.strategy';
import { JwtStrategy } from './use-cases/signin/jwt.strategy';
import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { UsersRepository } from '@/repositories/implementations/users.repository';
import { SigninController } from './use-cases/signin/signin.controller';
import { PrismaService } from '@/prisma.service';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: process.env.JWT_EXPIRES_IN },
    }),
  ],
  controllers: [SigninController],
  providers: [
    PrismaService,
    SigninService,
    LocalStrategy,
    JwtStrategy,
    {
      provide: IUsersRepository,
      useClass: UsersRepository,
    },
  ],
})
export class AuthModule {}
