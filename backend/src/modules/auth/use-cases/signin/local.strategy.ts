import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { ForbiddenException, Injectable } from '@nestjs/common';

import { SigninService } from './signin.service';
import { Message } from '@/messages';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private signinService: SigninService) {
    super({ usernameField: 'email' });
  }

  async validate(email: string, password: string): Promise<any> {
    const user = await this.signinService.validateUser({
      email,
      password,
    });

    if (!user) {
      throw new ForbiddenException({
        message: Message.USER_INVALID_CREDENTIALS,
      });
    }

    return user;
  }
}
