import { IsEmail, IsString } from 'class-validator';

export class RequestDto {
  @IsEmail()
  email: string;

  @IsString()
  password: string;
}
