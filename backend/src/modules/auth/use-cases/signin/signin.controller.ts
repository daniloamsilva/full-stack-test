import { Body, Controller, HttpCode, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { SigninService } from './signin.service';
import { RequestDto } from './request.dto';

@Controller('signin')
export class SigninController {
  constructor(private signinService: SigninService) {}

  @UseGuards(AuthGuard('local'))
  @Post()
  @HttpCode(200)
  async handle(@Body() { email, password }: RequestDto) {
    return await this.signinService.execute({ email, password });
  }
}
