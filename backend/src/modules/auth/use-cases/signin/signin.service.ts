import { ForbiddenException, Inject, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { RequestDto } from './request.dto';
import { IJwtPayload } from './payload.interface';
import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { Message } from '@/messages';

@Injectable()
export class SigninService {
  constructor(
    @Inject(IUsersRepository)
    private usersRepository: IUsersRepository,
    @Inject(JwtService)
    private jwtService: JwtService,
  ) {}

  async execute({ email }: RequestDto) {
    const user = await this.usersRepository.findByEmail(email);
    if (!user)
      throw new ForbiddenException({
        message: Message.USER_INVALID_CREDENTIALS,
      });

    const payload: IJwtPayload = {
      id: user.id,
      email,
    };
    return {
      jwt: this.jwtService.sign(payload),
    };
  }

  async validateUser({ email, password }: RequestDto): Promise<any> {
    const user = await this.usersRepository.findByEmail(email);
    if (user && (await bcrypt.compare(password, user.password))) {
      const { id, email } = user;
      return { id, email };
    }
    return null;
  }
}
