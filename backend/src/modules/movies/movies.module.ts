import { Module } from '@nestjs/common';

import { ListMoviesController } from './use-cases/list-movies/list-movies.controller';
import { ListMoviesService } from './use-cases/list-movies/list-movies.service';
import { PrismaService } from '@/prisma.service';
import { IMoviesRepository } from '@/repositories/interfaces/movies.repository.interface';
import { MoviesRepository } from '@/repositories/implementations/movies.repository';
import { CreateMovieController } from './use-cases/create-movie/create-movie.controller';
import { CreateMovieService } from './use-cases/create-movie/create-movie.service';
import { DeleteMovieService } from './use-cases/delete-movie/delete-movie.service';
import { DeleteMovieController } from './use-cases/delete-movie/delete-movie.controller';
import { UpdateMovieController } from './use-cases/update-movie/update-movie.controller';
import { UpdateMovieService } from './use-cases/update-movie/update-movie.service';

@Module({
  controllers: [
    ListMoviesController,
    CreateMovieController,
    DeleteMovieController,
    UpdateMovieController,
  ],
  providers: [
    PrismaService,
    ListMoviesService,
    CreateMovieService,
    DeleteMovieService,
    UpdateMovieService,
    {
      provide: IMoviesRepository,
      useClass: MoviesRepository,
    },
  ],
})
export class MoviesModule {}
