import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { CreateMovieService } from './create-movie.service';
import { RequestDto } from './request.dto';

@Controller('movies')
@UseGuards(AuthGuard('jwt'))
export class CreateMovieController {
  constructor(private createMovieService: CreateMovieService) {}

  @Post()
  async handle(@Body() request: RequestDto) {
    return this.createMovieService.execute(request);
  }
}
