import { Inject, Injectable } from '@nestjs/common';

import { MovieEntity } from '@/entities/movie.entity';
import { RequestDto } from './request.dto';
import { IMoviesRepository } from '@/repositories/interfaces/movies.repository.interface';

@Injectable()
export class CreateMovieService {
  constructor(
    @Inject(IMoviesRepository)
    private moviesRepository: IMoviesRepository,
  ) {}

  async execute({ title, year, poster }: RequestDto) {
    const newMovie = new MovieEntity();
    Object.assign(newMovie, { title, year: parseInt(year), poster });

    return await this.moviesRepository.save(newMovie);
  }
}
