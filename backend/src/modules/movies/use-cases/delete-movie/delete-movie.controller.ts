import { Controller, Delete, Param, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { DeleteMovieService } from './delete-movie.service';

@Controller('movies')
@UseGuards(AuthGuard('jwt'))
export class DeleteMovieController {
  constructor(private deleteMovieService: DeleteMovieService) {}

  @Delete(':id')
  async handle(@Param('id') id: string) {
    return this.deleteMovieService.execute(id);
  }
}
