import { IMoviesRepository } from '@/repositories/interfaces/movies.repository.interface';
import { Inject, Injectable } from '@nestjs/common';

@Injectable()
export class DeleteMovieService {
  constructor(
    @Inject(IMoviesRepository)
    private moviesRepository: IMoviesRepository,
  ) {}

  async execute(id: string): Promise<void> {
    await this.moviesRepository.delete(id);
  }
}
