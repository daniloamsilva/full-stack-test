import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ListMoviesService } from './list-movies.service';

@Controller('movies')
@UseGuards(AuthGuard('jwt'))
export class ListMoviesController {
  constructor(private listMoviesService: ListMoviesService) {}

  @Get()
  async handle(@Query('search') search: string, @Query('page') page: number) {
    return this.listMoviesService.execute({ search, page });
  }
}
