import { Inject, Injectable } from '@nestjs/common';
import { ResponseDto } from './response';
import { IMoviesRepository } from '@/repositories/interfaces/movies.repository.interface';

type ListMoviesProps = {
  search?: string;
  page?: number;
};

@Injectable()
export class ListMoviesService {
  constructor(
    @Inject(IMoviesRepository)
    private moviesRepository: IMoviesRepository,
  ) {}

  async execute({ search, page }: ListMoviesProps): Promise<ResponseDto> {
    const movies = await this.moviesRepository.searchByTitle(search);
    const totalPages = Math.ceil(movies.length / 10);
    const currentPage = page || 1;

    return {
      currentPage,
      totalPages,
      movies: movies.slice((currentPage - 1) * 10, currentPage * 10),
    };
  }
}
