import { MovieEntity } from '@/entities/movie.entity';

export type ResponseDto = {
  currentPage: number;
  totalPages: number;
  movies: MovieEntity[];
};
