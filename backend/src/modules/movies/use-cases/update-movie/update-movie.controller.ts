import { Body, Controller, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { UpdateMovieService } from './update-movie.service';
import { RequestDto } from './request.dto';

@Controller('movies')
@UseGuards(AuthGuard('jwt'))
export class UpdateMovieController {
  constructor(private updateMovieService: UpdateMovieService) {}

  @Put(':id')
  async handle(@Param('id') id: string, @Body() request: RequestDto) {
    return this.updateMovieService.execute(id, request);
  }
}
