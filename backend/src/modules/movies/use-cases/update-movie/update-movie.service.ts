import { IMoviesRepository } from '@/repositories/interfaces/movies.repository.interface';
import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { RequestDto } from './request.dto';
import { Message } from '@/messages';
import { MovieEntity } from '@/entities/movie.entity';

@Injectable()
export class UpdateMovieService {
  constructor(
    @Inject(IMoviesRepository)
    private moviesRepository: IMoviesRepository,
  ) {}

  async execute(id: string, data: RequestDto): Promise<MovieEntity> {
    const movie = await this.moviesRepository.findById(id);

    if (!movie) {
      throw new NotFoundException(Message.MOVIE_NOT_FOUND);
    }

    const updatedMovie = await this.moviesRepository.update(movie, {
      ...data,
      year: parseInt(data.year),
    });

    return updatedMovie;
  }
}
