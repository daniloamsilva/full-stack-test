import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { CreateUserService } from './create-user.service';
import { RequestDto } from './request.dto';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class CreateUserController {
  constructor(private createUserService: CreateUserService) {}

  @Post()
  async handle(@Body() request: RequestDto) {
    return this.createUserService.execute(request);
  }
}
