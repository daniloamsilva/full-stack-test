import { Inject, Injectable } from '@nestjs/common';

import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { RequestDto } from './request.dto';
import { UserEntity } from '@/entities/user.entity';

@Injectable()
export class CreateUserService {
  constructor(
    @Inject(IUsersRepository)
    private usersRepository: IUsersRepository,
  ) {}

  async execute({ email, password }: RequestDto) {
    const newUser = new UserEntity();
    Object.assign(newUser, { email, password });

    return await this.usersRepository.save(newUser);
  }
}
