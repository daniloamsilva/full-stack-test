import { Controller, Delete, Param, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { DeleteUserService } from './delete-user.service';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class DeleteUserController {
  constructor(private deleteUserService: DeleteUserService) {}

  @Delete(':id')
  async handle(@Param('id') id: string) {
    return this.deleteUserService.execute(id);
  }
}
