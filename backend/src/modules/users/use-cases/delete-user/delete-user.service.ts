import { Inject, Injectable } from '@nestjs/common';

import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';

@Injectable()
export class DeleteUserService {
  constructor(
    @Inject(IUsersRepository)
    private usersRepository: IUsersRepository,
  ) {}

  async execute(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
