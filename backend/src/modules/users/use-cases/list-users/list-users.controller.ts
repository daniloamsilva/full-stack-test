import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ListUsersService } from './list-users.service';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class ListUsersController {
  constructor(private listUsersService: ListUsersService) {}

  @Get()
  async handle(@Query('page') page: number) {
    return this.listUsersService.execute({ page });
  }
}
