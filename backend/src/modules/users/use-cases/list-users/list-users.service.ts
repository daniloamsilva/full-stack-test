import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { Inject, Injectable } from '@nestjs/common';

type ListUsersProps = {
  page: number;
};

@Injectable()
export class ListUsersService {
  constructor(
    @Inject(IUsersRepository)
    private usersRepository: IUsersRepository,
  ) {}

  async execute({ page }: ListUsersProps) {
    const users = await this.usersRepository.findAll();
    const totalPages = Math.ceil(users.length / 10);
    const currentPage = page || 1;

    return {
      currentPage,
      totalPages,
      users: users.slice((currentPage - 1) * 10, currentPage * 10),
    };
  }
}
