import { Body, Controller, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { UpdateUserService } from './update-user.service';
import { RequestDto } from './request.dto';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class UpdateUserController {
  constructor(private updateUserService: UpdateUserService) {}

  @Put(':id')
  async handle(@Param('id') id: string, @Body() request: RequestDto) {
    return this.updateUserService.execute(id, request);
  }
}
