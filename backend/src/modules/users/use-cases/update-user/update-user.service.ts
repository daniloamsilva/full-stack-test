import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { RequestDto } from './request.dto';
import { Message } from '@/messages';
import { UserEntity } from '@/entities/user.entity';

@Injectable()
export class UpdateUserService {
  constructor(
    @Inject(IUsersRepository)
    private usersRepository: IUsersRepository,
  ) {}

  async execute(id: string, data: RequestDto): Promise<UserEntity> {
    const user = await this.usersRepository.findById(id);

    if (!user) {
      throw new NotFoundException(Message.USER_NOT_FOUND);
    }

    return this.usersRepository.update(user, data);
  }
}
