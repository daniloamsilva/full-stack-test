import { Module } from '@nestjs/common';
import { ListUsersController } from './use-cases/list-users/list-users.controller';
import { ListUsersService } from './use-cases/list-users/list-users.service';
import { PrismaService } from '@/prisma.service';
import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { UsersRepository } from '@/repositories/implementations/users.repository';
import { CreateUserController } from './use-cases/create-user/create-user.controller';
import { CreateUserService } from './use-cases/create-user/create-user.service';
import { UpdateUserController } from './use-cases/update-user/update-user.controller';
import { UpdateUserService } from './use-cases/update-user/update-user.service';
import { DeleteUserController } from './use-cases/delete-user/delete-user.controller';
import { DeleteUserService } from './use-cases/delete-user/delete-user.service';

@Module({
  controllers: [
    ListUsersController,
    CreateUserController,
    UpdateUserController,
    DeleteUserController,
  ],
  providers: [
    PrismaService,
    ListUsersService,
    CreateUserService,
    UpdateUserService,
    DeleteUserService,
    {
      provide: IUsersRepository,
      useClass: UsersRepository,
    },
  ],
})
export class UsersModule {}
