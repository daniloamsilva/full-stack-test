import { Inject } from '@nestjs/common';
import {
  IMoviesRepository,
  UpdateProps,
} from '../interfaces/movies.repository.interface';
import { PrismaService } from '@/prisma.service';
import { MovieEntity } from '@/entities/movie.entity';

export class MoviesRepository implements IMoviesRepository {
  constructor(
    @Inject(PrismaService)
    private prisma: PrismaService,
  ) {}

  async searchByTitle(search: string): Promise<MovieEntity[]> {
    let movies = await this.prisma.movie.findMany({
      where: {},
      orderBy: { title: 'asc' },
    });

    if (search) {
      movies = movies.filter((movie) =>
        movie.title.toLowerCase().includes(search.toLowerCase()),
      );
    }

    const moviesFormatted = movies.map((movie) => {
      const movieFormatted = new MovieEntity();
      Object.assign(movieFormatted, movie);
      return movieFormatted;
    });

    return moviesFormatted;
  }

  async save(movie: MovieEntity): Promise<MovieEntity> {
    const movieCreated = await this.prisma.movie.create({
      data: {
        title: movie.title,
        poster: movie.poster,
        year: movie.year,
      },
    });

    const movieFormatted = new MovieEntity();
    Object.assign(movieFormatted, movieCreated);
    return movieFormatted;
  }

  async delete(id: string): Promise<void> {
    await this.prisma.movie.delete({ where: { id } });
  }

  async update(movie: MovieEntity, data: UpdateProps): Promise<MovieEntity> {
    const movieUpdated = await this.prisma.movie.update({
      where: { id: movie.id },
      data,
    });

    const movieFormatted = new MovieEntity();
    Object.assign(movieFormatted, movieUpdated);
    return movieFormatted;
  }

  async findById(id: string): Promise<MovieEntity | undefined> {
    const movie = await this.prisma.movie.findUnique({ where: { id } });
    if (!movie) return undefined;

    const movieFormatted = new MovieEntity();
    Object.assign(movieFormatted, movie);
    return movieFormatted;
  }
}
