import { Inject } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

import { UserEntity } from '@/entities/user.entity';
import { IUsersRepository } from '../interfaces/users.repository.interface';
import { PrismaService } from '@/prisma.service';

export class UsersRepository implements IUsersRepository {
  constructor(@Inject(PrismaService) private prisma: PrismaService) {}

  async findByEmail(email: string): Promise<UserEntity | undefined> {
    return this.prisma.user.findUnique({ where: { email } });
  }

  async save(user: UserEntity): Promise<UserEntity> {
    await this.prisma.user.create({
      data: {
        ...user,
        password: await bcrypt.hash(user.password, 10),
      },
    });
    return user;
  }

  async findAll(): Promise<UserEntity[]> {
    return this.prisma.user.findMany({ orderBy: { email: 'asc' } });
  }

  async deleteAll(): Promise<void> {
    await this.prisma.user.deleteMany();
  }

  async findById(id: string): Promise<UserEntity> {
    return this.prisma.user.findUnique({ where: { id } });
  }

  async update(
    user: UserEntity,
    data: Partial<UserEntity>,
  ): Promise<UserEntity> {
    const updatedUser = this.prisma.user.update({
      where: { id: user.id },
      data: {
        ...data,
        password: data.password
          ? await bcrypt.hash(data.password, 10)
          : undefined,
      },
    });

    const updatedUserEntity = new UserEntity();
    Object.assign(updatedUserEntity, updatedUser);

    return updatedUserEntity;
  }

  async delete(id: string): Promise<void> {
    await this.prisma.user.delete({ where: { id } });
  }
}
