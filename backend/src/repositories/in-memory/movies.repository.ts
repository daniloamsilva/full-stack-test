import { MovieEntity } from '@/entities/movie.entity';
import {
  IMoviesRepository,
  UpdateProps,
} from '../interfaces/movies.repository.interface';

export class MoviesRepository implements IMoviesRepository {
  movies: MovieEntity[] = [];

  async save(movie: MovieEntity): Promise<MovieEntity> {
    this.movies.push(movie);
    return movie;
  }

  async searchByTitle(search: string): Promise<MovieEntity[]> {
    if (!search) return this.movies;
    return this.movies.filter((movie) => movie.title.includes(search));
  }

  async delete(id: string): Promise<void> {
    this.movies = this.movies.filter((movie) => movie.id !== id);
  }

  async update(movie: MovieEntity, data: UpdateProps): Promise<MovieEntity> {
    const movieIndex = this.movies.findIndex((m) => m.id === movie.id);
    this.movies[movieIndex] = {
      ...movie,
      ...data,
    };
    return this.movies[movieIndex];
  }

  async findById(id: string): Promise<MovieEntity> {
    return this.movies.find((movie) => movie.id === id);
  }
}
