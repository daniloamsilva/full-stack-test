import * as bcrypt from 'bcrypt';

import { UserEntity } from 'src/entities/user.entity';
import { IUsersRepository } from '../interfaces/users.repository.interface';

export class UsersRepository implements IUsersRepository {
  private users: UserEntity[] = [];

  async findByEmail(email: string): Promise<UserEntity | undefined> {
    return this.users.find((user) => user.email === email);
  }

  async save(user: UserEntity): Promise<UserEntity> {
    this.users.push({
      ...user,
      password: await bcrypt.hash(user.password, 10),
    });
    return user;
  }

  async findAll(): Promise<UserEntity[]> {
    return this.users;
  }

  async deleteAll(): Promise<void> {
    this.users = [];
  }

  async findById(id: string): Promise<UserEntity> {
    return this.users.find((user) => user.id === id);
  }

  async update(
    user: UserEntity,
    data: Partial<UserEntity>,
  ): Promise<UserEntity> {
    const userIndex = this.users.findIndex((u) => u.id === user.id);
    this.users[userIndex] = {
      ...user,
      ...data,
    };
    return this.users[userIndex];
  }

  async delete(id: string): Promise<void> {
    this.users = this.users.filter((user) => user.id !== id);
  }
}
