import { MovieEntity } from '@/entities/movie.entity';

export type UpdateProps = {
  title?: string;
  year?: number;
  poster?: string;
};

export abstract class IMoviesRepository {
  abstract save(movie: MovieEntity): Promise<MovieEntity>;
  abstract searchByTitle(search: string): Promise<MovieEntity[]>;
  abstract delete(id: string): Promise<void>;
  abstract update(movie: MovieEntity, data: UpdateProps): Promise<MovieEntity>;
  abstract findById(id: string): Promise<MovieEntity>;
}
