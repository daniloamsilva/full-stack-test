import { UserEntity } from 'src/entities/user.entity';

export abstract class IUsersRepository {
  abstract findByEmail(email: string): Promise<UserEntity | undefined>;
  abstract save(user: UserEntity): Promise<UserEntity>;
  abstract findAll(): Promise<UserEntity[]>;
  abstract deleteAll(): Promise<void>;
  abstract findById(id: string): Promise<UserEntity | undefined>;
  abstract update(
    user: UserEntity,
    data: Partial<UserEntity>,
  ): Promise<UserEntity>;
  abstract delete(id: string): Promise<void>;
}
