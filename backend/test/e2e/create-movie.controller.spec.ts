import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';
import { MoviesModule } from '@/modules/movies/movies.module';

describe('CreateMovieController', () => {
  let app: INestApplication;
  let token: string;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, MoviesModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    const userLogin = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    token = userLogin.body.jwt;
  });

  it('should be able to create a new movie', async () => {
    const response = await request(app.getHttpServer())
      .post('/movies')
      .send({
        title: 'Movie 1',
        year: 1994,
        poster: 'https://example.com/poster.jpg',
      })
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(201);
  });
});
