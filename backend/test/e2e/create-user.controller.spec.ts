import * as request from 'supertest';
import { ClassSerializerInterceptor, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';
import { UsersModule } from '@/modules/users/users.module';
import { Reflector } from '@nestjs/core';
import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';

describe('CreateUserController', () => {
  let app: INestApplication;
  let usersRepository: IUsersRepository;
  let token: string;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, UsersModule],
    }).compile();

    app = module.createNestApplication();
    app.useGlobalInterceptors(
      new ClassSerializerInterceptor(app.get(Reflector)),
    );
    await app.init();

    usersRepository = module.get<IUsersRepository>(IUsersRepository);

    const userLogin = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    token = userLogin.body.jwt;
  });

  afterAll(async () => {
    await usersRepository.deleteAll();
    await app.close();
  });

  it('should be able to create a new user', async () => {
    const response = await request(app.getHttpServer())
      .post('/users')
      .send({
        email: 'johndoe@email.com',
        password: 'pass1234',
      })
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(201);
    expect(response.body).not.toHaveProperty('password');
  });
});
