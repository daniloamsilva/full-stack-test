import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';
import { MoviesModule } from '@/modules/movies/movies.module';
import { MovieEntity } from '@/entities/movie.entity';
import { IMoviesRepository } from '@/repositories/interfaces/movies.repository.interface';

describe('DeleteMovieController', () => {
  let app: INestApplication;
  let movies: MovieEntity[];
  let token: string;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, MoviesModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    const moviesRepository = module.get<IMoviesRepository>(IMoviesRepository);
    movies = await moviesRepository.searchByTitle('');

    const userLogin = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    token = userLogin.body.jwt;
  });

  it('should be able to delete a movie', async () => {
    const response = await request(app.getHttpServer())
      .delete(`/movies/${movies[0].id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
  });
});
