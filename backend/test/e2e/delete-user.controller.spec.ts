import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';
import { UsersModule } from '@/modules/users/users.module';
import { UserEntity } from '@/entities/user.entity';
import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';

describe('DeleteUserController', () => {
  let app: INestApplication;
  let user: UserEntity;
  let token: string;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, UsersModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    const usersRepository = module.get<IUsersRepository>(IUsersRepository);
    user = await usersRepository.findByEmail('admin@admin.com');

    const userLogin = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    token = userLogin.body.jwt;
  });

  it('should be able to delete a user', async () => {
    const response = await request(app.getHttpServer())
      .delete(`/users/${user.id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
  });
});
