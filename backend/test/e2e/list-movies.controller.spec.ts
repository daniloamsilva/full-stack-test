import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';
import { MoviesModule } from '@/modules/movies/movies.module';

describe('ListMoviesController', () => {
  let app: INestApplication;
  let token: string;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, MoviesModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    const userLogin = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    token = userLogin.body.jwt;
  });

  it('should be able to search all movies', async () => {
    const response = await request(app.getHttpServer())
      .get('/movies')
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
  });

  it('should be able to search movies by title', async () => {
    const response = await request(app.getHttpServer())
      .get('/movies')
      .query({ search: 'The' })
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
  });

  it('should be able to paginate the movies', async () => {
    const response = await request(app.getHttpServer())
      .get('/movies')
      .query({ page: 2 })
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
  });
});
