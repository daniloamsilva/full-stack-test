import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';

describe('SigninController', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();
  });

  it('should be able to signin', async () => {
    const response = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    expect(response.status).toBe(200);
  });

  it('should not be able to signin with a non-existing user', async () => {
    const response = await request(app.getHttpServer()).post('/signin').send({
      email: 'nonexisting@email.com',
      password: 'pass1234',
    });

    expect(response.status).toBe(403);
  });

  it('should not be able to signin with an invalid password', async () => {
    const response = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'wrongpassword',
    });

    expect(response.status).toBe(403);
  });
});
