import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';
import { MoviesModule } from '@/modules/movies/movies.module';
import { IMoviesRepository } from '@/repositories/interfaces/movies.repository.interface';
import { MovieEntity } from '@/entities/movie.entity';

describe('UpdateMovieController', () => {
  let app: INestApplication;
  let token: string;
  let movies: MovieEntity[];

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, MoviesModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    const moviesRepository = module.get<IMoviesRepository>(IMoviesRepository);
    movies = await moviesRepository.searchByTitle('');

    const userLogin = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    token = userLogin.body.jwt;
  });

  it('should be able to update a movie', async () => {
    const movie = movies[0];

    const response = await request(app.getHttpServer())
      .put(`/movies/${movie.id}`)
      .send({
        title: 'Movie 1 Edited',
        year: 1995,
        poster: 'https://example.com/posteredited.jpg',
      })
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
  });

  it('should not be able to update a movie that does not exist', async () => {
    const response = await request(app.getHttpServer())
      .put('/movies/invalid-id')
      .send({
        title: 'Movie 1 Edited',
        year: 1995,
        poster: 'https://example.com/posteredited.jpg',
      })
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(404);
  });
});
