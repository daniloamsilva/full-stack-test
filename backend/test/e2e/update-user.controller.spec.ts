import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { AuthModule } from '@/modules/auth/auth.module';
import { UsersModule } from '@/modules/users/users.module';
import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { UserEntity } from '@/entities/user.entity';

describe('UpdateUserController', () => {
  let app: INestApplication;
  let token: string;
  let user: UserEntity;
  let usersRepository: IUsersRepository;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, UsersModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    usersRepository = module.get<IUsersRepository>(IUsersRepository);
    user = await usersRepository.findByEmail('admin@admin.com');

    const userLogin = await request(app.getHttpServer()).post('/signin').send({
      email: 'admin@admin.com',
      password: 'admin',
    });

    token = userLogin.body.jwt;
  });

  afterAll(async () => {
    await usersRepository.deleteAll();
  });

  it('should be able to update a user', async () => {
    const response = await request(app.getHttpServer())
      .put(`/users/${user.id}`)
      .send({
        email: 'edited@email.com',
        password: 'edited-password',
      })
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
  });

  it('should not be able to update a user that does not exist', async () => {
    const response = await request(app.getHttpServer())
      .put('/users/invalid-id')
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(404);
  });
});
