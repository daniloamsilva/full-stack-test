import { CreateMovieService } from '@/modules/movies/use-cases/create-movie/create-movie.service';
import { MoviesRepository } from '@/repositories/in-memory/movies.repository';

describe('CreateMovieService', () => {
  let createMovieService: CreateMovieService;

  beforeEach(() => {
    const moviesRepository = new MoviesRepository();
    createMovieService = new CreateMovieService(moviesRepository);
  });

  it('should be able to create a new movie', async () => {
    const result = await createMovieService.execute({
      title: 'Movie 1',
      year: '1994',
      poster: 'https://example.com/poster.jpg',
    });

    expect(result).toHaveProperty('id');
  });
});
