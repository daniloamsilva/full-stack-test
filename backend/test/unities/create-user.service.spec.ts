import { CreateUserService } from '@/modules/users/use-cases/create-user/create-user.service';
import { UsersRepository } from '@/repositories/in-memory/users.repository';

describe('CreateUserService', () => {
  let createUserService: CreateUserService;

  beforeEach(() => {
    const usersRepository = new UsersRepository();
    createUserService = new CreateUserService(usersRepository);
  });

  it('should be able to create a new user', async () => {
    const result = await createUserService.execute({
      email: 'johndoe@email.com',
      password: 'pass1234',
    });

    expect(result).toHaveProperty('id');
  });
});
