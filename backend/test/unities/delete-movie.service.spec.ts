import { MovieEntity } from '@/entities/movie.entity';
import { DeleteMovieService } from '@/modules/movies/use-cases/delete-movie/delete-movie.service';
import { MoviesRepository } from '@/repositories/in-memory/movies.repository';

describe('DeleteMovieService', () => {
  let deleteMovieService: DeleteMovieService;
  let movie: MovieEntity;

  beforeEach(() => {
    const moviesRepository = new MoviesRepository();
    deleteMovieService = new DeleteMovieService(moviesRepository);

    movie = new MovieEntity();
    Object.assign(movie, {
      title: 'any_title',
      poster: 'any_poster',
      year: 2021,
    });
  });

  it('should delete a movie', async () => {
    const movieSaved = await deleteMovieService.execute(movie.id);
    expect(movieSaved).toBeUndefined();
  });
});
