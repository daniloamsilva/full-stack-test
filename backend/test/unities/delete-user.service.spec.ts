import * as bcrypt from 'bcrypt';

import { UserEntity } from '@/entities/user.entity';
import { DeleteUserService } from '@/modules/users/use-cases/delete-user/delete-user.service';
import { UsersRepository } from '@/repositories/in-memory/users.repository';

describe('DeleteUserService', () => {
  let deleteUserService: DeleteUserService;
  let user: UserEntity;

  beforeEach(() => {
    const usersRepository = new UsersRepository();
    deleteUserService = new DeleteUserService(usersRepository);

    user = new UserEntity();
    Object.assign(user, {
      email: 'johndoe@email.com',
      password: bcrypt.hashSync('123456', 8),
    });
  });

  it('should delete a user', async () => {
    const userSaved = await deleteUserService.execute(user.id);
    expect(userSaved).toBeUndefined();
  });
});
