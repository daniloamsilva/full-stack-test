import { MovieEntity } from '@/entities/movie.entity';
import { ListMoviesService } from '@/modules/movies/use-cases/list-movies/list-movies.service';
import { MoviesRepository } from '@/repositories/in-memory/movies.repository';

describe('ListMoviesService', () => {
  let listMoviesService: ListMoviesService;

  beforeEach(() => {
    const moviesRepository = new MoviesRepository();
    listMoviesService = new ListMoviesService(moviesRepository);

    for (let i = 1; i <= 20; i++) {
      const newMovie = new MovieEntity();
      Object.assign(newMovie, {
        title: `Movie ${i}`,
        year: 1994,
        poster: 'https://example.com/poster.jpg',
      });

      moviesRepository.save(newMovie);
    }
  });

  it('should be able to search all movies', async () => {
    const result = await listMoviesService.execute({});

    expect(result.movies).toHaveLength(10);
    expect(result.currentPage).toBe(1);
    expect(result.totalPages).toBe(2);
  });

  it('should be able to search movies by title', async () => {
    const result = await listMoviesService.execute({ search: 'Movie 10' });

    expect(result.movies).toHaveLength(1);
    expect(result.currentPage).toBe(1);
    expect(result.totalPages).toBe(1);
  });

  it('should be able to paginate the movies', async () => {
    const result = await listMoviesService.execute({ page: 2 });

    expect(result.movies).toHaveLength(10);
    expect(result.movies[0].title).toBe('Movie 11');
    expect(result.currentPage).toBe(2);
    expect(result.totalPages).toBe(2);
  });
});
