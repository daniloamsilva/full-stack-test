import { UserEntity } from '@/entities/user.entity';
import { ListUsersService } from '@/modules/users/use-cases/list-users/list-users.service';
import { UsersRepository } from '@/repositories/in-memory/users.repository';

describe('ListUsersService', () => {
  let listUsersService: ListUsersService;

  beforeEach(async () => {
    const usersRepository = new UsersRepository();
    listUsersService = new ListUsersService(usersRepository);

    for (let i = 1; i <= 20; i++) {
      const newUser = new UserEntity();
      Object.assign(newUser, {
        email: `user${i}@email.com`,
        password: `user${i}password`,
      });

      await usersRepository.save(newUser);
    }
  });

  it('should be able to search all users', async () => {
    const result = await listUsersService.execute({ page: 1 });

    expect(result.users).toHaveLength(10);
    expect(result.totalPages).toBe(2);
    expect(result.currentPage).toBe(1);
  });
});
