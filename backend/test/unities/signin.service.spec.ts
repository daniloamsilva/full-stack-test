import { ForbiddenException, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';

import { SigninService } from '@/modules/auth/use-cases/signin/signin.service';
import { IUsersRepository } from '@/repositories/interfaces/users.repository.interface';
import { UserEntity } from '@/entities/user.entity';
import { UsersRepository } from '@/repositories/in-memory/users.repository';
import { LocalStrategy } from '@/modules/auth/use-cases/signin/local.strategy';
import { JwtStrategy } from '@/modules/auth/use-cases/signin/jwt.strategy';

describe('SigninService', () => {
  let app: INestApplication;
  let signinService: SigninService;
  let usersRepository: IUsersRepository;
  let user: UserEntity;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        PassportModule,
        JwtModule.register({
          secret: process.env.JWT_SECRET,
          signOptions: { expiresIn: process.env.JWT_EXPIRES_IN },
        }),
      ],
      providers: [
        SigninService,
        LocalStrategy,
        JwtStrategy,
        {
          provide: IUsersRepository,
          useClass: UsersRepository,
        },
      ],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    signinService = module.get<SigninService>(SigninService);
    usersRepository = module.get<IUsersRepository>(IUsersRepository);

    const newUser = new UserEntity();
    Object.assign(newUser, {
      email: 'johndoe@email.com',
      password: 'pass1234',
    });

    user = await usersRepository.save(newUser);
  });

  it('should be able to signin', async () => {
    const result = await signinService.execute({
      email: user.email,
      password: 'pass1234',
    });

    expect(result).toMatchObject({ jwt: expect.any(String) });
  });

  it('should not be able to signin with a non-existing user', async () => {
    await expect(
      signinService.execute({
        email: 'nonexisting@email.com',
        password: 'pass1234',
      }),
    ).rejects.toThrow(ForbiddenException);
  });

  it('should not be able to signin with an invalid password', async () => {
    const result = await signinService.validateUser({
      email: user.email,
      password: 'invalidpassword',
    });

    expect(result).toBe(null);
  });
});
