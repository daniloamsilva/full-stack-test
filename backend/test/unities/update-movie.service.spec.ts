import { NotFoundException } from '@nestjs/common';

import { MovieEntity } from '@/entities/movie.entity';
import { UpdateMovieService } from '@/modules/movies/use-cases/update-movie/update-movie.service';
import { MoviesRepository } from '@/repositories/in-memory/movies.repository';

describe('UpdateMovieService', () => {
  let updateMovieService: UpdateMovieService;
  let movie: MovieEntity;

  beforeEach(async () => {
    const moviesRepository = new MoviesRepository();
    updateMovieService = new UpdateMovieService(moviesRepository);

    movie = new MovieEntity();
    Object.assign(movie, {
      title: 'Movie 1',
      year: 1994,
      poster: 'https://example.com/poster.jpg',
    });
    await moviesRepository.save(movie);
  });

  it('should be able to update a new movie', async () => {
    const result = await updateMovieService.execute(movie.id, {
      title: 'Movie 1 Updated',
      year: '1995',
      poster: 'https://example.com/poster-updated.jpg',
    });

    expect(result).toHaveProperty('id');
    expect(result.title).toBe('Movie 1 Updated');
    expect(result.year).toBe(1995);
    expect(result.poster).toBe('https://example.com/poster-updated.jpg');
  });

  it('should not be able to update a movie that does not exist', () => {
    expect(async () => {
      await updateMovieService.execute('invalid-id', {
        title: 'Movie 1 Updated',
        year: '1995',
        poster: 'https://example.com/poster-updated.jpg',
      });
    }).rejects.toThrow(NotFoundException);
  });
});
