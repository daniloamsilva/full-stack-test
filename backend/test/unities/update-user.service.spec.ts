import * as bcrypt from 'bcrypt';
import { NotFoundException } from '@nestjs/common';

import { UserEntity } from '@/entities/user.entity';
import { UpdateUserService } from '@/modules/users/use-cases/update-user/update-user.service';
import { UsersRepository } from '@/repositories/in-memory/users.repository';

describe('UpdateUserService', () => {
  let updateUserService: UpdateUserService;
  let user: UserEntity;

  beforeEach(async () => {
    const usersRepository = new UsersRepository();
    updateUserService = new UpdateUserService(usersRepository);

    user = new UserEntity();
    Object.assign(user, {
      email: 'johndoe@email.com',
      password: bcrypt.hashSync('password', 10),
    });
    await usersRepository.save(user);
  });

  it('should be able to update a new user', async () => {
    const result = await updateUserService.execute(user.id, {
      email: 'edited@email.com',
      password: 'edited-password',
    });

    expect(result).toHaveProperty('id');
    expect(result.email).toBe('edited@email.com');
  });

  it('should not be able to update a user that does not exist', () => {
    expect(async () => {
      await updateUserService.execute('invalid-id', {});
    }).rejects.toThrow(NotFoundException);
  });
});
