import { api } from "@/lib/axios";

import { Movie } from "@/types/movie";

interface CreateMovieRequest {
  title: string;
  year: number;
  poster: string;
}

interface CreateMovieResponse {
  data: Movie[];
}

export async function createMovie(
  data: CreateMovieRequest
): Promise<CreateMovieResponse> {
  return await api.post("/movies", data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
    },
  });
}
