import { api } from "@/lib/axios";

import { User } from "@/types/user";

interface CreateUserRequest {
  email: string;
  password: string;
}

interface CreateUserResponse {
  data: User;
}

export async function createUser(
  data: CreateUserRequest
): Promise<CreateUserResponse> {
  return await api.post("/users", data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
    },
  });
}
