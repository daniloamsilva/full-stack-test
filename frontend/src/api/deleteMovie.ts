import { api } from "@/lib/axios";

interface DeletMovieRequest {
  id: string;
}

export async function deleteMovie({ id }: DeletMovieRequest): Promise<void> {
  return await api.delete(`/movies/${id}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
    },
  });
}
