import { api } from "@/lib/axios";

interface DeletUserRequest {
  id: string;
}

export async function deleteUser({ id }: DeletUserRequest): Promise<void> {
  return await api.delete(`/users/${id}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
    },
  });
}
