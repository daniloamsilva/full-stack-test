import { api } from "@/lib/axios";
import { User } from "@/types/user";

interface ListUsersRequest {
  page: number;
}

interface ListUsersResponse {
  data: {
    users: User[];
    currentPage: string;
    totalPages: number;
  };
}

export async function listUsers({
  page,
}: ListUsersRequest): Promise<ListUsersResponse> {
  return await api.get("/users", {
    params: {
      page,
    },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
    },
  });
}
