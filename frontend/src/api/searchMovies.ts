import { api } from "@/lib/axios";

import { Movie } from "@/types/movie";

interface SearchMoviesRequest {
  page?: number;
}

interface SearchMoviesResponse {
  data: {
    currentPage: string;
    totalPages: number;
    movies: Movie[];
  };
}

export async function searchMovies({
  page,
}: SearchMoviesRequest): Promise<SearchMoviesResponse> {
  return await api.get("/movies", {
    params: {
      page: page || 1,
    },
    headers: {
      Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
    },
  });
}
