import { api } from "@/lib/axios";

export interface SignInBody {
  email: string;
  password: string;
}

export interface SignInResponse {
  data: {
    jwt: string;
  };
}

export async function signIn({
  email,
  password,
}: SignInBody): Promise<SignInResponse> {
  return api.post("/signin", { email, password });
}
