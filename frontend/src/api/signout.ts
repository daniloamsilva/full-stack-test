export async function signOut() {
  localStorage.removeItem("watchmovies:jwt");
}
