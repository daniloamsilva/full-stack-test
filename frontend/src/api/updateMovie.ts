import { api } from "@/lib/axios";

import { Movie } from "@/types/movie";

interface UpdateMovieRequest {
  id: string;
  title: string;
  year: number;
  poster: string;
}

interface UpdateMovieResponse {
  data: Movie;
}

export async function updateMovie(
  data: UpdateMovieRequest
): Promise<UpdateMovieResponse> {
  return await api.put(
    `/movies/${data.id}`,
    {
      title: data.title,
      year: data.year,
      poster: data.poster,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
      },
    }
  );
}
