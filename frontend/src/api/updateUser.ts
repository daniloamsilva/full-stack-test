import { api } from "@/lib/axios";

import { User } from "@/types/user";

interface UpdateUserRequest {
  id: string;
  email: string;
  password: string;
}

interface UpdateUserResponse {
  data: User;
}

export async function updateUser(
  data: UpdateUserRequest
): Promise<UpdateUserResponse> {
  return await api.put(
    `/users/${data.id}`,
    {
      email: data.email,
      password: data.password ? data.password : undefined,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("watchmovies:jwt")}`,
      },
    }
  );
}
