import { useEffect } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';

import { Header } from '@/components/header';

export function AppLayout() {
    const navigate = useNavigate();

    useEffect(() => {
        const logged = localStorage.getItem('watchmovies:jwt');

        if (!logged) {
            navigate('/signin');
        }
    });
        
    return (
        <div className='flex min-h-screen flex-col antialiased'>
            <Header />

            <div className='flex flex-1 flex-col gap-4 p-8 pt-6'>
                <Outlet />
            </div>
        </div>

    )
}