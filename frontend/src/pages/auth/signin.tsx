import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { useMutation } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';

import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@radix-ui/react-label";
import { toast } from "sonner"
import { signIn } from '@/api/signin';

const signInForm = z.object({
    email: z.string().email(),
    password: z.string().min(5),
});

type SignInForm = z.infer<typeof signInForm>;

export function Signin() {
    const { register, handleSubmit, formState: { isSubmitting } } = useForm<SignInForm>();
    const navigate = useNavigate();

    const { mutateAsync: signin } = useMutation({
        mutationFn: signIn,
    });

    async function handleSignin(data: SignInForm) {
        try {
            const response = await signin({ email: data.email, password: data.password })
            localStorage.setItem('watchmovies:jwt', response.data.jwt)
            toast.success('Login realizado com sucesso!')
            navigate("/");
          } catch (error) {
            toast.error('Credenciais inválidas.')
          }
    }

    return (
        <div className="w-[350px] flex flex-col justify-center gap-6">
            <div className="flex flex-col gap-2 text-center">
                <h1 className="text-2xl font-semibold tracking-tight">Entre na plataforma</h1>
                <p className="text-sm text-muted-foreground">Assista a seus filmes favoritos!</p>
            </div>

            <form onSubmit={handleSubmit(handleSignin)} className="space-y-4">
                <div className="space-y-2">
                    <Label htmlFor="email">Email</Label>
                    <Input type="email" id="email" placeholder="Digite seu email" {...register('email')} />
                </div>
                <div className="space-y-2">
                    <Label htmlFor="password">Senha</Label>
                    <Input type="password" id="password" placeholder="Digite sua senha" {...register('password')} />
                </div>

                <Button disabled={isSubmitting} type="submit" className="w-full">Entrar</Button>
            </form>
        </div>
    )
}