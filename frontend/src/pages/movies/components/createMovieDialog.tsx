import { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { useMutation } from "@tanstack/react-query";
import { toast } from "sonner";
import { Plus } from "lucide-react";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { createMovie } from "@/api/createMovie";
import { Label } from "@radix-ui/react-label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";

const createMovieForm = z.object({
  title: z.string(),
  year: z.number(),
  poster: z.string(),
});

type CreateMovieForm = z.infer<typeof createMovieForm>;

type CreateMovieDialogProps = {
  searchMoviesFn: (data: { page: number }) => void;
};

export function CreateMovieDialog({ searchMoviesFn }: CreateMovieDialogProps) {
  const [createMovieDialogOpen, setCreateMovieDialogOpen] =
    useState<boolean>(false);
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
    reset,
  } = useForm<CreateMovieForm>();

  const { mutateAsync: createMoviesFn } = useMutation({
    mutationFn: createMovie,
    onSuccess: () => {
      searchMoviesFn({ page: 1 });
      toast.success("Filme criado com sucesso");
    },
  });

  async function handleCreateMovie(data: CreateMovieForm) {
    try {
      createMoviesFn({
        title: data.title,
        year: data.year,
        poster: data.poster,
      });
      setCreateMovieDialogOpen(false);
      reset();
    } catch (error) {
      toast.error("Preencha todos os campos corretamente.");
    }
  }

  return (
    <Dialog
      open={createMovieDialogOpen}
      onOpenChange={setCreateMovieDialogOpen}
    >
      <DialogTrigger className="flex items-center justify-around border p-2 rounded">
        <Plus size={20} />
        <span className="ml-2">Novo filme</span>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Novo filme</DialogTitle>
          <DialogDescription>
            <form
              onSubmit={handleSubmit(handleCreateMovie)}
              className="space-y-4 mt-4"
            >
              <div className="flex flex-col space-y-2">
                <Label htmlFor="title">Título</Label>
                <Input
                  type="text"
                  id="title"
                  placeholder="Digite o nome do filme"
                  {...register("title")}
                />
              </div>
              <div className="flex flex-col space-y-2">
                <Label htmlFor="year">Ano</Label>
                <Input
                  type="number"
                  id="year"
                  placeholder="1994"
                  {...register("year")}
                />
              </div>
              <div className="flex flex-col space-y-2">
                <Label htmlFor="poster">Poster</Label>
                <Input
                  type="text"
                  id="poster"
                  placeholder="Digite a url do poster"
                  {...register("poster")}
                />
              </div>
              <div className="flex justify-end">
                <Button disabled={isSubmitting} type="submit">
                  Salvar
                </Button>
              </div>
            </form>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}
