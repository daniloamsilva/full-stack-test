import { useState } from "react";
import { useMutation } from "@tanstack/react-query";
import { toast } from "sonner";
import { Trash2 } from "lucide-react";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { deleteMovie } from "@/api/deleteMovie";
import { Button } from "@/components/ui/button";
import { Movie } from "@/types/movie";
import { useForm } from "react-hook-form";

type DeleteMovieDialogProps = {
  movie: Movie;
  searchMoviesFn: (data: { page: number }) => void;
};

export function DeleteMovieDialog({
  movie,
  searchMoviesFn,
}: DeleteMovieDialogProps) {
  const [deleteMovieDialogOpen, setDeleteMovieDialogOpen] =
    useState<boolean>(false);
  const {
    handleSubmit,
    formState: { isSubmitting },
  } = useForm();

  const { mutateAsync: deleteMoviesFn } = useMutation({
    mutationFn: deleteMovie,
    onSuccess: () => {
      searchMoviesFn({ page: 1 });
      toast.success("Filme removido com sucesso");
    },
  });

  async function handleDeleteMovie(movie: Movie) {
    try {
      deleteMoviesFn({ id: movie.id });
      setDeleteMovieDialogOpen(false);
    } catch (error) {
      toast.error("Ops! Algo deu errado, tente novamente mais tarde.");
    }
  }

  return (
    <Dialog
      open={deleteMovieDialogOpen}
      onOpenChange={setDeleteMovieDialogOpen}
    >
      <DialogTrigger asChild>
        <Button
          className="w-full rounded-none rounded-br-lg"
          variant="destructive"
        >
          <Trash2 size={20} />
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Remover filme</DialogTitle>
          <DialogDescription>
            <form
              onSubmit={handleSubmit(() => handleDeleteMovie(movie))}
              className="space-y-4 mt-4"
            >
              <p>Dejesa remover o filme: "{movie.title}"?</p>
              <div className="flex justify-end">
                <Button
                  disabled={isSubmitting}
                  type="submit"
                  variant="destructive"
                >
                  Remover
                </Button>
              </div>
            </form>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}
