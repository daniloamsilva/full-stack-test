import { useEffect, useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { useMutation } from "@tanstack/react-query";
import { toast } from "sonner";
import { Pencil } from "lucide-react";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { updateMovie } from "@/api/updateMovie";
import { Label } from "@radix-ui/react-label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { Movie } from "@/types/movie";

const updateMovieForm = z.object({
  title: z.string(),
  year: z.number(),
  poster: z.string(),
});

type UpdateMovieForm = z.infer<typeof updateMovieForm>;

type UpdateMovieDialogProps = {
  movie: Movie;
  searchMoviesFn: (data: { page: number }) => void;
  currentPage: number;
};

export function UpdateMovieDialog({
  movie,
  searchMoviesFn,
  currentPage,
}: UpdateMovieDialogProps) {
  const [updateMovieDialogOpen, setUpdateMovieDialogOpen] =
    useState<boolean>(false);
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
    reset,
  } = useForm<UpdateMovieForm>();

  const { mutateAsync: updateMoviesFn } = useMutation({
    mutationFn: updateMovie,
    onSuccess: () => {
      searchMoviesFn({ page: currentPage });
      toast.success("Filme editado com sucesso");
    },
  });

  async function handleUpdateMovie(data: UpdateMovieForm) {
    try {
      updateMoviesFn({
        id: movie.id,
        title: data.title,
        year: data.year,
        poster: data.poster,
      });
      setUpdateMovieDialogOpen(false);
      reset();
    } catch (error) {
      toast.error("Preencha todos os campos corretamente.");
    }
  }

  useEffect(() => {
    reset({
      title: movie.title,
      year: movie.year,
      poster: movie.poster,
    });
  }, [movie, reset]);

  return (
    <Dialog
      open={updateMovieDialogOpen}
      onOpenChange={setUpdateMovieDialogOpen}
    >
      <DialogTrigger asChild>
        <Button
          className="w-full rounded-none rounded-bl-lg"
          variant="secondary"
        >
          <Pencil size={20} />
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Editar filme</DialogTitle>
          <DialogDescription>
            <form
              onSubmit={handleSubmit(handleUpdateMovie)}
              className="space-y-4 mt-4"
            >
              <div className="flex flex-col space-y-2">
                <Label htmlFor="title">Título</Label>
                <Input
                  type="text"
                  id="title"
                  placeholder="Digite o nome do filme"
                  {...register("title")}
                />
              </div>
              <div className="flex flex-col space-y-2">
                <Label htmlFor="year">Ano</Label>
                <Input
                  type="number"
                  id="year"
                  placeholder="1994"
                  {...register("year")}
                />
              </div>
              <div className="flex flex-col space-y-2">
                <Label htmlFor="poster">Poster</Label>
                <Input
                  type="text"
                  id="poster"
                  placeholder="Digite a url do poster"
                  {...register("poster")}
                />
              </div>
              <div className="flex justify-end">
                <Button disabled={isSubmitting} type="submit">
                  Salvar
                </Button>
              </div>
            </form>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}
