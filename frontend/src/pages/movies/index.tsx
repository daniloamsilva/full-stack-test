import { ChevronLeft, ChevronRight } from "lucide-react";
import { useMutation } from "@tanstack/react-query";

import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { searchMovies } from "@/api/searchMovies";
import { useEffect, useState } from "react";
import { Movie } from "@/types/movie";
import { Button } from "@/components/ui/button";
import { CreateMovieDialog } from "./components/createMovieDialog";
import { DeleteMovieDialog } from "./components/deleteMovieDialog";
import { UpdateMovieDialog } from "./components/updateMovieDialog";

export function Movies() {
  const [movies, setMovies] = useState<Movie[]>([]);
  const [page, setPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);

  const { mutateAsync: searchMoviesFn } = useMutation({
    mutationFn: searchMovies,
    onSuccess: (data) => {
      setMovies(data.data.movies);
      setPage(parseInt(data.data.currentPage));
      setTotalPages(data.data.totalPages);
    },
  });

  useEffect(() => {
    searchMoviesFn({ page });
  }, []);

  return (
    <>
      <div className="flex items-center justify-between w-full">
        <div className="flex items-center">
          <h1 className="text-3xl font-bold tracking-tight mr-4">Filmes</h1>
          <CreateMovieDialog searchMoviesFn={searchMoviesFn} />
        </div>
        <div>
          <Button
            variant="outline"
            size="icon"
            disabled={page === 1}
            onClick={() => searchMoviesFn({ page: page - 1 })}
          >
            <ChevronLeft className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100" />
          </Button>
          <Button
            variant="outline"
            size="icon"
            disabled={page === totalPages}
            onClick={() => searchMoviesFn({ page: page + 1 })}
          >
            <ChevronRight className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100" />
          </Button>
        </div>
      </div>
      <main className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-4">
        {movies.map((movie) => (
          <Card key={movie.id} className="flex flex-col justify-between">
            <CardHeader>
              <img src={movie.poster} alt={movie.title} />
            </CardHeader>
            <CardContent>
              <CardTitle>{movie.title}</CardTitle>
              <CardDescription>{movie.year}</CardDescription>
            </CardContent>
            <div className="flex items-center justify-center">
              <UpdateMovieDialog
                movie={movie}
                searchMoviesFn={searchMoviesFn}
                currentPage={page}
              />
              <DeleteMovieDialog
                movie={movie}
                searchMoviesFn={searchMoviesFn}
              />
            </div>
          </Card>
        ))}
      </main>
    </>
  );
}
