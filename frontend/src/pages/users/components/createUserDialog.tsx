import { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { useMutation } from "@tanstack/react-query";
import { toast } from "sonner";
import { Plus } from "lucide-react";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { createUser } from "@/api/createUser";
import { Label } from "@radix-ui/react-label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";

const createUserForm = z.object({
  email: z.string(),
  password: z.string().min(5),
});

type CreateUserForm = z.infer<typeof createUserForm>;

type CreateUserDialogProps = {
  listUsersFn: (data: { page: number }) => void;
};

export function CreateUserDialog({ listUsersFn }: CreateUserDialogProps) {
  const [createUserDialogOpen, setCreateUserDialogOpen] =
    useState<boolean>(false);
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
    reset,
  } = useForm<CreateUserForm>();

  const { mutateAsync: createUsersFn } = useMutation({
    mutationFn: createUser,
    onSuccess: () => {
      listUsersFn({ page: 1 });
      toast.success("Usuário criado com sucesso");
    },
  });

  async function handleCreateUser(data: CreateUserForm) {
    try {
      createUsersFn({
        email: data.email,
        password: data.password,
      });
      setCreateUserDialogOpen(false);
      reset();
    } catch (error) {
      toast.error("Preencha todos os campos corretamente.");
    }
  }

  return (
    <Dialog open={createUserDialogOpen} onOpenChange={setCreateUserDialogOpen}>
      <DialogTrigger className="flex items-center justify-around border p-2 rounded">
        <Plus size={20} />
        <span className="ml-2">Novo usuário</span>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Novo Usuário</DialogTitle>
          <DialogDescription>
            <form
              onSubmit={handleSubmit(handleCreateUser)}
              className="space-y-4 mt-4"
            >
              <div className="flex flex-col space-y-2">
                <Label htmlFor="title">Email</Label>
                <Input
                  type="email"
                  placeholder="Digite o email do usuário"
                  {...register("email")}
                />
              </div>
              <div className="flex flex-col space-y-2">
                <Label htmlFor="year">Senha</Label>
                <Input
                  type="password"
                  min={5}
                  placeholder="Crie uma senha para o usuário"
                  {...register("password")}
                />
              </div>
              <div className="flex justify-end">
                <Button disabled={isSubmitting} type="submit">
                  Salvar
                </Button>
              </div>
            </form>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}
