import { useState } from "react";
import { useMutation } from "@tanstack/react-query";
import { toast } from "sonner";
import { Trash2 } from "lucide-react";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { deleteUser } from "@/api/deleteUser";
import { Button } from "@/components/ui/button";
import { User } from "@/types/user";
import { useForm } from "react-hook-form";

type DeleteUserDialogProps = {
  user: User;
  listUsersFn: (data: { page: number }) => void;
};

export function DeleteUserDialog({ user, listUsersFn }: DeleteUserDialogProps) {
  const [deleteUserDialogOpen, setDeleteUserDialogOpen] =
    useState<boolean>(false);
  const {
    handleSubmit,
    formState: { isSubmitting },
  } = useForm();

  const { mutateAsync: deleteUsersFn } = useMutation({
    mutationFn: deleteUser,
    onSuccess: () => {
      listUsersFn({ page: 1 });
      toast.success("Usuário removido com sucesso");
    },
  });

  async function handleDeleteUser(user: User) {
    try {
      deleteUsersFn({ id: user.id });
      setDeleteUserDialogOpen(false);
    } catch (error) {
      toast.error("Ops! Algo deu errado, tente novamente mais tarde.");
    }
  }

  return (
    <Dialog open={deleteUserDialogOpen} onOpenChange={setDeleteUserDialogOpen}>
      <DialogTrigger asChild>
        <Button variant="destructive" size="xs">
          <Trash2 className="mr-2 h-3 w-3" />
          Remover
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Remover usuário</DialogTitle>
          <DialogDescription>
            <form
              onSubmit={handleSubmit(() => handleDeleteUser(user))}
              className="space-y-4 mt-4"
            >
              <p>Dejesa remover o usuário: "{user.email}"?</p>
              <div className="flex justify-end">
                <Button
                  disabled={isSubmitting}
                  type="submit"
                  variant="destructive"
                >
                  Remover
                </Button>
              </div>
            </form>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}
