import {
  ChevronLeft,
  ChevronRight,
  ChevronsLeft,
  ChevronsRight,
} from "lucide-react";

import { Button } from "../../../components/ui/button";

interface PaginationProps {
  page: number;
  totalCount: number;
  totalPages: number;
  listUsersFn: (data: { page: number }) => void;
}

export function Pagination({
  page,
  totalPages,
  totalCount,
  listUsersFn,
}: PaginationProps) {
  return (
    <div className="flex items-center justify-between">
      <span className="text-sm text-muted-foreground">
        Total de {totalCount} item(s)
      </span>

      <div className="flex items-center gap-6 lg:gap-8">
        <div className="text-sm font-medium">
          Página {page} de {totalPages}
        </div>
        <div className="flex items-center gap-2">
          <Button
            disabled={page == 1}
            onClick={() => listUsersFn({ page: 1 })}
            variant="outline"
            className="h-8 w-8 p-0"
          >
            <ChevronsLeft className="h-4 w-4" />
            <span className="sr-only">Primeira página</span>
          </Button>
          <Button
            disabled={page == 1}
            onClick={() => listUsersFn({ page: page - 1 })}
            variant="outline"
            className="h-8 w-8 p-0"
          >
            <ChevronLeft className="h-4 w-4" />
            <span className="sr-only">Página anterior</span>
          </Button>
          <Button
            disabled={page == totalPages}
            onClick={() => listUsersFn({ page: page + 1 })}
            variant="outline"
            className="h-8 w-8 p-0"
          >
            <ChevronRight className="h-4 w-4" />
            <span className="sr-only">Próxima página</span>
          </Button>
          <Button
            disabled={page == totalPages}
            onClick={() => listUsersFn({ page: totalPages })}
            variant="outline"
            className="h-8 w-8 p-0"
          >
            <ChevronsRight className="h-4 w-4" />
            <span className="sr-only">Última página</span>
          </Button>
        </div>
      </div>
    </div>
  );
}
