import { useEffect, useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { useMutation } from "@tanstack/react-query";
import { toast } from "sonner";
import { Pencil } from "lucide-react";

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { updateUser } from "@/api/updateUser";
import { Label } from "@radix-ui/react-label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { User } from "@/types/user";

const updateUserForm = z.object({
  email: z.string(),
  password: z.string(),
});

type UpdateUserForm = z.infer<typeof updateUserForm>;

type UpdateUserDialogProps = {
  user: User;
  listUsersFn: (data: { page: number }) => void;
  currentPage: number;
};

export function UpdateUserDialog({
  user,
  listUsersFn,
  currentPage,
}: UpdateUserDialogProps) {
  const [updateUserDialogOpen, setUpdateUserDialogOpen] =
    useState<boolean>(false);
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
    reset,
  } = useForm<UpdateUserForm>();

  const { mutateAsync: updateUsersFn } = useMutation({
    mutationFn: updateUser,
    onSuccess: () => {
      listUsersFn({ page: currentPage });
      toast.success("Usuário editado com sucesso");
    },
  });

  async function handleUpdateUser(data: UpdateUserForm) {
    try {
      updateUsersFn({
        id: user.id,
        email: data.email,
        password: data.password,
      });
      setUpdateUserDialogOpen(false);
      reset();
    } catch (error) {
      toast.error("Preencha todos os campos corretamente.");
    }
  }

  useEffect(() => {
    reset({
      email: user.email,
    });
  }, [user, reset]);

  return (
    <Dialog open={updateUserDialogOpen} onOpenChange={setUpdateUserDialogOpen}>
      <DialogTrigger asChild>
        <Button variant="secondary" size="xs">
          <Pencil className="mr-2 h-3 w-3" />
          Editar
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Editar filme</DialogTitle>
          <DialogDescription>
            <form
              onSubmit={handleSubmit(handleUpdateUser)}
              className="space-y-4 mt-4"
            >
              <div className="flex flex-col space-y-2">
                <Label htmlFor="title">Email</Label>
                <Input
                  type="email"
                  placeholder="Digite o email do usuário"
                  {...register("email")}
                />
              </div>
              <div className="flex flex-col space-y-2">
                <Label htmlFor="year">Senha</Label>
                <Input
                  type="password"
                  min={5}
                  placeholder="Crie uma senha para o usuário"
                  {...register("password")}
                />
              </div>
              <div className="flex justify-end">
                <Button disabled={isSubmitting} type="submit">
                  Salvar
                </Button>
              </div>
            </form>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>
  );
}
