import { useMutation } from "@tanstack/react-query";

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { Pagination } from "@/pages/users/components/pagination";
import { useEffect, useState } from "react";
import { User } from "@/types/user";
import { listUsers } from "@/api/list-users";
import { CreateUserDialog } from "./components/createUserDialog";
import { DeleteUserDialog } from "./components/deleteUserDialog";
import { UpdateUserDialog } from "./components/updateUserDialog";

export function Users() {
  const [users, setUsers] = useState<User[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);

  const { mutateAsync: listUsersFn } = useMutation({
    mutationFn: listUsers,
    onSuccess: (data) => {
      setUsers(data.data.users);
      setCurrentPage(parseInt(data.data.currentPage));
      setTotalPages(data.data.totalPages);
    },
  });

  useEffect(() => {
    listUsersFn({ page: currentPage });
  }, []);

  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="flex items-center">
          <h1 className="text-3xl font-bold tracking-tight mr-4">Usuários</h1>
          <CreateUserDialog listUsersFn={listUsersFn} />
        </div>

        <div className="space-y-2.5">
          <div className="rounded-md border">
            <Table>
              <TableHeader>
                <TableRow>
                  <TableHead className="w-[140px]">Identificador</TableHead>
                  <TableHead>Email</TableHead>
                  <TableHead className="w-[164px]"></TableHead>
                  <TableHead className="w-[132px]"></TableHead>
                </TableRow>
              </TableHeader>
              <TableBody>
                {users.map((user) => {
                  return (
                    <TableRow key={user.id}>
                      <TableCell className="font-mono text-xs font-medium">
                        {user.id}
                      </TableCell>
                      <TableCell className="font-medium">
                        {user.email}
                      </TableCell>
                      <TableCell className="text-end">
                        <UpdateUserDialog
                          user={user}
                          listUsersFn={listUsersFn}
                          currentPage={currentPage}
                        />
                      </TableCell>
                      <TableCell>
                        <DeleteUserDialog
                          user={user}
                          listUsersFn={listUsersFn}
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </div>
        </div>

        <Pagination
          page={currentPage}
          totalCount={users.length}
          totalPages={totalPages}
          listUsersFn={listUsersFn}
        />
      </div>
    </>
  );
}
