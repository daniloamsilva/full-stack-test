import { createBrowserRouter } from "react-router-dom";

import { Movies } from "./pages/movies";
import { Signin } from "./pages/auth/signin";
import { AppLayout } from "./pages/_layouts/app";
import { AuthLayout } from "./pages/_layouts/auth";
import { Users } from "./pages/users";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <AppLayout />,
    children: [
      { path: "/", element: <Movies /> },
      { path: "/users", element: <Users /> },
    ],
  },
  {
    path: "/",
    element: <AuthLayout />,
    children: [{ path: "/signin", element: <Signin /> }],
  },
]);
